### To access other software (eg. vlc, mpv), Add RPM Fusion Package Repository:
	#First, update the DNF package repository cache with the following command:
		$ sudo dnf makecache

	#To install the RPM Fusion Free repository package, run the following command:
		$ sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm

	#To install the RPM Fusion Non-Free repository package, run the following command:
		$ sudo dnf install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
		$ sudo dnf makecache

### To remove the XDG_... dirs like ~/Templates, ~/Public...
	/etc/xdg/user-dirs.conf  and change: enabled=False
	/etc/xdg/user-dirs.defaults and alter as you like.
Also
	~/.config/user-dirs.dirs and comment out what you dont want.

### Add alias and exports to: ~/.bashrc, rest of aliases come from system wide /etc/profile.d/ files...

### To check installed packages: $ dnf history userinstalled

### To see available and installed groups: $ dnf grouplist -v hidden

### To install a group: $ dnf groupinstall "Name of Group"

### $ dnf upgrade 			#To update & upgrade packages
### $ dnf check-update  		#To check 
### $ dnf search [all] term... 		#To search packages
### $ dnf list (global-expression)      #Filtering results with glob expressions
	eg. $ dnf list kernel*-4*
### $ dnf dnf list all			#Lists all installed and available packages
### $ dnf list installed		#Lists installed packages only
### $ dnf list available		#Lists all available packages in all 
					 enabled repositories.
### $ dnf group list			#Lists all package groups.
### $ dnf repolist			#Listing enabled repositories.
### $ dnf repository-packages repo_id list #Listing packages from a single repository
### $ dnf info package_name…		#Displaying Package Information
###   To be continued.... 


# Disable tracker-miner-fs-3 it is very heavy on resources From (https://www.linuxuprising.com/2019/07/how-to-completely-disable-tracker.html):

   systemctl --user mask tracker-extract-3.service tracker-miner-fs-3.service tracker-miner-rss-3.service tracker-writeback-3.service tracker-xdg-portal-3.service tracker-miner-fs-control-3.service

	then

	tracker3 reset -s -r

	reboot

# to view and remove metadata from files use exiftool:
	man exiftool 

# to speed up dnf edit /etc/dnf/dnf.conf:
  and add:
	fastestmirror=True
	deltarpm=True
	max_parallel_downloads=3

# To convert video to Android 480x320:
		ffmpeg -i nameofvideo.xxx -s 480X320 output.mkv
		yt-dlp -f '\''bv*[height=480]+ba'\' URL (For Closest Resolution to 480X320)

# The Polkit ERROR messege that prevents access to drives...etc:
   sudo chmod +s /usr/lib/polkit-1/polkit-agent-helper-1 
FIXES IT.

# To search for a WORD within many PDF files:
	pdfgrep -nm 5 WORD *.pdf #-m 5 means as soon as it finds 5 matches on
				 #a pdf it moves to the next.

# To Trim off from FILEblabla-whatever.m4v:
Inside the directory type:
	for f in ./*; do mv "$f" "${f%-*.m4v}.m4v" ; done
# To rename part of file with empty:
	rename "part...." "" *

# To add a playlist in Cmus:

    :pl-create my-playlist
    3 to navigate to the playlists window
    Highlight the new playlist and press space to mark it.
    :add -p ~/Music/artist/ to add songs from that directory to the marked playlist.

# To create a template for Vim:

  Create ~/.vim/skeleton/diary.d (the template you want to create)

# Add to ~/.vimrc to create a personalized "diary/template":

  " For d (Diary) files:
  autocmd BufNewFile *.d 0r ~/.vim/skeleton/diary.d 
  " This populates all .d files with the template.

  " Make <F9> paste current date:
  nnoremap <F9> :r !date<cr>

# Script to create a file with current date for diary:
  Place in $PATH: ~/.local/bin

  File: ~/local/bin/diary and ~/local/bin/diario:

  #!/bin/bash

  vim "/home/joe/.vim/diary/diario_$(date +%F).d"

# Gimagereader: To read and save TEXT contained in an IMAGE.
