set nocompatible
filetype plugin on
syntax on

" Specific for C Programming
"  function return type on its own line so that the function name begins on column 0
set cinoptions+=t0
" put braces around case body
set cinoptions+=l1
" You should invoke your build with :make so that it populates the quickfix list, 
" allowing you to quickly navigate between errors and warnings (:cn, :cp, :cc). 
" I like to use autowrite since saving manually before building is tedious:
set autowrite
" I also configure make to build as much as possible and in parallel with all my cores:
set makeprg=make\ -kj$(nproc)
" End of C Programming


" Plugins will be downloaded under the specified directory.
call plug#begin(has('nvim') ? stdpath('data') . '/plugged' : '~/.vim/plugged')

" Declare the list of plugins.
Plug 'tpope/vim-sensible'
Plug 'junegunn/seoul256.vim'

" List ends here. Plugins become visible to Vim after this call.
call plug#end()
